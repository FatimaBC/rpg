package rpg;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
public class RPG 
{
    public static void main(String[] args) 
    {
        Magasin magasin=new Magasin();
        Couteau couteau=new Couteau();
        Epee epee=new Epee();
        Marteau marteau=new Marteau();
        //Ajout armes au magasin
        magasin.addArme(epee);
        magasin.addArme(couteau);
        magasin.addArme(marteau);
        //Début du jeu
        System.out.println("-------\n"
                + "Début du jeu\n"
                +"-------"
                );
        //création héro
        System.out.println("Création de ton héro");
        Scanner sc = new Scanner(System.in);
        System.out.println("Nom de ton héro:");
        String str = sc.nextLine();
        Hero Hero=new Hero(str);
        //Récapitulatif du Héro
        System.out.println(Hero.Description());
        if(Hero.getMesArmes().isEmpty()) //ACHETER UNE ARME
        {
            Scanner sc2 = new Scanner(System.in);
            System.out.println("Vous n'avez pas d'arme en poche. Acheter une arme ? O/N");
            String str2 = sc2.nextLine();
            if(str2.equals("O"))
            {
               magasin.vendreArme(Hero);
               
            }                
        }
        //Création autres éléments
        Monstre Monstre=new Monstre();
        Obstacle ob=new Obstacle();
        Maison m=new Maison();
        ArrayList<Object> list=new ArrayList<Object>();
        list.add(Monstre);
        list.add(ob);
        list.add(m);
        list.add(magasin);
        //Parcours du Héro 
        while(Hero.getPv()>0)
        {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(list.size());
            Object objet=list.get(randomInt);
            if (objet instanceof Maison)
            {
                Scanner sc3 = new Scanner(System.in);
                System.out.println("Il y a une maison !! Se reposer ? O/N");
                String str3 = sc3.nextLine();
                switch (str3)
                {
                    case "O": 
                        Scanner sc4 = new Scanner(System.in);
                        System.out.println("Combien de temps ? (1/2/3)");
                        int str4 = sc4.nextInt();
                        Hero.seReposer(str4);
                        break;
                    case "N":
                        System.out.println("Vous avez refusé.");
                        break;
                }
            }
            else if(objet instanceof Obstacle)
            {
                Scanner sc5 = new Scanner(System.in);
                System.out.println("Il y a un obstacle! Le détruire ? O/N");
                String str5 = sc5.nextLine();
                switch (str5)
                {
                    case "O": 
                        Hero.detruire((Obstacle) objet);
                        list.remove(objet);
                        break;
                    case "N":
                        System.out.println("Vous avez contourné l'obstacle.");
                        break;
                }
            }
            else if(objet instanceof Magasin)
            {
                Scanner sc6 = new Scanner(System.in);
                System.out.println("Il y a un magasin ! Acheter une nouvelle arme ? O/N");
                String str6 = sc6.nextLine();
                switch (str6)
                {
                    case "O": 
                         magasin.vendreArme(Hero);                  
                        break;
                    case "N":
                        System.out.println("Vous n'êtes pas rentré dans le magasin.");
                        break;
                }
            }
            else if (objet instanceof Monstre)
            {
                ((Monstre) objet).apparaitre();
                Scanner sc8 = new Scanner(System.in);
                System.out.println("1- Fuir \n"
                        + "2- Combattre");
                int str8 = sc8.nextInt();
                switch (str8)
                {
                    case 1:
                        System.out.println("Vous avez fuit !");
                        break;
                    case 2:                  
                        while (Monstre.getPv()>0 || Hero.getPv()>0)
                        {                            
                            if(Monstre.getPv()==0)
                            break;
                            else if (Hero.getPv()==0)
                            break;
                            else
                            {
                                Monstre.frapper(Hero);
                                if(Monstre.getPv()==0)
                                {
                                    Monstre.mourrir();
                                    break;
                                }
                                
                                else if (Hero.getPv()==0)
                                break;
                                Hero.frapper(Monstre, Hero.getMonArme());
                            }
                        }                       
                        break;
                }               
            }
        }
        if (Hero.getPv()<0)
        {
            System.out.println("Vous avez perdu !");
        }
    }  
}
