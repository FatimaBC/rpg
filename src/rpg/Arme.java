package rpg;
public abstract class Arme {
    private int force;
    private float prix;
    public Arme()
    {
        this.force=3;
        this.prix=10;
    }
    public Arme(int uneForce, float unPrix)
    {
        this.force=uneForce;
        this.prix=unPrix;
    }
   public int getForce()
   {
       return this.force;
   }
   public float getPrix()
   {
       return this.prix;
   }
   abstract String getNomArme();
}
