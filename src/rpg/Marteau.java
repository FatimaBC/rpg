package rpg;
public class Marteau extends Arme {
    public Marteau()
    {
        super();
    }
     public void attack(Monstre m)
    {
        System.out.println("Il a attacké le monstre");
    }
    public void attack(Obstacle o)
    {
        System.out.println("Il a attacké l'obstacle");
    }
    public void attack(Destructible d)
    {
        System.out.println("Il a attacké le destructible");
    }
    public String getNomArme()
    {
        return "marteau";
    }
}
