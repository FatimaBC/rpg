package rpg;
public class Personnage 
{
    private int xp;
    private int pv;
    private String prenom;
    
    public Personnage()
    {
        this.pv=100;
        this.xp=0;
    }
    public Personnage(String unPrenom)
    {
        this.prenom=unPrenom;
        this.pv=100;
        this.xp=0;
    }
    public Personnage(String unPrenom, int unXp)
    {
        this.prenom=unPrenom;
        this.xp=unXp;
        this.pv=100;
    }
    public void mourrir()
    {
        System.out.println(this.prenom+" est mort!");
    }
    public int getPv()
    {
        return this.pv;
    }
    public void setPv(int unPv)
    {
        if(unPv<0)
        {
            unPv=0;
        }
        this.pv=unPv;
    }
    public String getPrenom()
    {
        return this.prenom;
    }
    public void setPrenom(String unPrenom)
    {
        this.prenom=unPrenom;
    }
    public String Description()
    {
        return "Prenom: "+this.prenom+"\n"
                + "Pv: "+this.pv;
    }
}
