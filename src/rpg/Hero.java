package rpg;
import java.util.ArrayList;
public class Hero extends Personnage
{
   private float money=100;
   private ArrayList<Arme> mesArmes;
   private Arme monArme;
   public Hero()
   {
       super();
       super.setPrenom("Hero");
       this.mesArmes=new ArrayList<Arme>();
       this.monArme=new Marteau();
       this.mesArmes.add(this.monArme);
   }
   public Hero(String unPrenom)
   {
       super(unPrenom);
       this.mesArmes=new ArrayList<Arme>();
       this.monArme=new Marteau();
       this.mesArmes.add(this.monArme);
   }
   public Hero(String unPrenom, int unXp)
   {
       super(unPrenom, unXp);
       this.mesArmes=new ArrayList<Arme>();
       this.monArme=new Marteau();
       this.mesArmes.add(this.monArme);
   }
   public void estTouche(Monstre unMonstre)
   {
       super.setPv(super.getPv()-unMonstre.getForce());
       System.out.println("Tu as été touché!! PV: "+super.getPv());
   }
   public void frapper(Monstre unMonstre, Arme uneArme)
   {
       unMonstre.estTouche(uneArme, this);
       System.out.println("Vous avez frappé le monstre ! Il lui reste "
       +unMonstre.getPv()+" PV"
               );
   }
   public void detruire(Obstacle unobstacle)
   {
       System.out.println("L'obstacle est détruit!");
   }
   public void seReposer(int tps)
   {
       if (tps<1)
       {
           tps=1;
       }
       if (tps>3)
       {
           tps=3;
       }
       super.setPv(super.getPv()+tps);
       System.out.println("Vous avez récupéré "+tps+"PV.");
   }
   public void AcheterArme(Arme uneArme)
   {
       float lePrix=uneArme.getPrix();
       if(this.money<lePrix)
       {
           System.out.println("Tu n'as pas assez de sous !");
       }
       else{
           this.money-=lePrix;
           this.mesArmes.add(uneArme);
           System.out.println("Tu as bien acheté ton arme : "+uneArme.getNomArme()
           +" il te reste "+this.getMoney()+" pieces d'or");
       }
   }
   public void choisirMonArme(Arme uneArme)
   {
       this.monArme=uneArme;
   }
   public Arme getMonArme()
   {
       return this.monArme;
   }
   public ArrayList<Arme> getMesArmes()
   {
       return this.mesArmes;
   }
   public float getMoney()
   {
       return this.money;
   }
   public String getMesArmesStr()
   {
       if(this.mesArmes==null)
       {
           return "Pas d'armes";
       }
       else
       {
        String armes="";
        for(Arme uneArme:this.mesArmes)
        {
            armes+=uneArme.getNomArme()+"\n";
        }
        return armes;
       }
   }
   public String Description()
   {
       return  "Ta fiche description: \n"
               +"-------------------\n"
               +super.Description()+"\n"
               + "Argent: "+this.money+"\n"
               + "Mes armes: "+this.getMesArmesStr()
               +"Mon arme actuelle: "+this.getMonArme().getNomArme()
               +"\n-------------------";
       
   }
}
