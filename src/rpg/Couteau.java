package rpg;
public class Couteau extends Arme{
    public Couteau()
    {
        super();
    }
    public Couteau (int uneForce, float unPrix)
    {
        super(uneForce, unPrix);
    }
    public String getNomArme()
    {
        return "couteau";
    }
}
