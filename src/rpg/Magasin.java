package rpg;

import java.util.ArrayList;
import java.util.Scanner;

public class Magasin  {
     ArrayList<Arme> armes;
    public Magasin()
    {
        super();
        armes=new ArrayList<Arme>();
    }
    public void vendreArme(Hero H)
    {
        System.out.println("Les armes en magasins:");
        Scanner sc3 = new Scanner(System.in);
        for(int i=0; i<armes.size(); i++)
        {
            System.out.println(i+"-"+armes.get(i).getNomArme()+"-prix: "+armes.get(i).getPrix());
        }
        int str3 = sc3.nextInt();
        H.AcheterArme(armes.get(str3));
        if(H.getMesArmes().isEmpty())
        {
           H.choisirMonArme(armes.get(str3));
        }
    }        
    public void addArme(Arme uneArme)
    {
        armes.add(uneArme);
    }
    public ArrayList<Arme> getArmes()
    {
        return armes;
    }
}
