package rpg;
public class Monstre extends Personnage
{
    private int force=3;
    
    public Monstre()
    {
        super();
        super.setPrenom("Monstre");
    }
    public Monstre(String unPrenom, int unXp)
    {
        super(unPrenom, unXp);
    }
    public Monstre(String unPrenom,int unXp, int uneForce)
    {
        super(unPrenom, unXp);
        this.force=uneForce;
    }
    public void apparaitre()
    {
        System.out.println("Un monstre apparaît !");
    }
    public void frapper(Hero unHero)
    {
        System.out.println(super.getPrenom()+" vous attaque !");
        unHero.estTouche(this);
        
    }
    public void estTouche(Arme uneArme, Hero unHero)
    {
       super.setPv(super.getPv()-uneArme.getForce());
       System.out.println("Le monstre est touché !! PV: "+super.getPv());
    }
    public int getForce()
    {
        return this.force;
    }   
    public String Description()
    {
        return super.Description()+"\n"
                +"Force d'impact: "+this.force;
    }
}
