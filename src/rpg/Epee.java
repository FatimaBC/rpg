package rpg;
public class Epee extends Arme {
     public Epee()
    {
        super();
    }
    public Epee (int uneForce, float unPrix)
    {
        super(uneForce, unPrix);
    }
    public String getNomArme()
    {
        return "épée";
    }
}
